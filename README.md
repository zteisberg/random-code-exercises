# random-code-exercises

## The general purpose of this repository is to store code used to solve various programming excercises found on the web. These are just a fun hobby of mine and helps keep me sharp on algorithms while also letting me try new language features, libraries, or design patterns.

## The code here isn't designed to be perfect, nor will it be overly optimized. In general what I'm going for are reasonably readable solutions with reasonable runtimes.

| Current Sites Included                         |
| ---------------------------------------------- |
| [Project Euler](https://projecteuler.net/)     |