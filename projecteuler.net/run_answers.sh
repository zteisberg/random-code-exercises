for f in *.py
do
  echo ""
  echo "/---------------\\"
  echo "Answer for $f"
  echo "\\---------------/"
  # take action on each file. $f store current file name
  python $f
done