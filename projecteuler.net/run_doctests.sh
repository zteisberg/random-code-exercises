for f in *.py
do
  echo ""
  echo "/------------------------\\"
  echo "Tests on $f functions."
  echo "\\------------------------/"
  # take action on each file. $f store current file name
  python -m doctest -v $f
done