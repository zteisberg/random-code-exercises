from p_1 import is_product_multiple_of
from typing import Generator

def fibonacci(n: int) -> int:
    """
    https://projecteuler.net/problem=2
    How one would get an arbitrary position in the fibonacci sequence.
    Generally with fibonacci you see 1, 1, 2 as the beginning, but in the problem they skip a 1, so we do here too.
    >>> fibonacci(1)
    1
    >>> fibonacci(2)
    2
    >>> fibonacci(10)
    89
    """
    previous_value = 0
    current_value = 1
    for _ in range(0, n):
        f_n_minus_2 = previous_value
        f_n_minus_1 = current_value
        current_value = f_n_minus_2 + f_n_minus_1
        previous_value = f_n_minus_1
    return current_value

def fibonacci_generator(limit: int = 10) -> Generator[int, None, None]:
    """
    Given an integer limit, produce the values in the fibonacci sequence up to but not including
    that limit
    Examples:
    >>> list(fibonacci_generator(40))
    [1, 2, 3, 5, 8, 13, 21, 34]
    >>> list(fibonacci_generator(8))
    [1, 2, 3, 5]
    >>> list(fibonacci_generator(10))
    [1, 2, 3, 5, 8]
    """
    previous_value = 0
    current_value = 1
    while (previous_value + current_value) < limit:
        f_n_minus_2 = previous_value
        f_n_minus_1 = current_value
        current_value = f_n_minus_2 + f_n_minus_1
        yield current_value
        previous_value = f_n_minus_1

if __name__ == '__main__':
    even_terms = [product for product in fibonacci_generator(4000000) if is_product_multiple_of(product, 2)]
    print(sum(even_terms))

# Answer: 4613732