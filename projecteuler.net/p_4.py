from typing import Iterable, Generator
from itertools import tee, islice

def is_palindrome(n: int) -> bool:
    """
    Specifically checks if an integer is the same if represented backwards as it is forwards
    
    Examples:
    >>> is_palindrome(404)
    True
    >>> is_palindrome(403)
    False
    >>> is_palindrome(555)
    True
    >>> is_palindrome(595)
    True
    >>> is_palindrome(100)
    False
    """
    n = str(n)
    return n == n[::-1]

def iterable_unique_combination_generator(iterator: Iterable) -> Generator:
    """
    Takes one iterator, and finds all the possible unique unordered combinations it could
    have with itself.

    Examples:
    >>> list(iterable_unique_combination_generator(range(0,5)))
    [(0, 0), (0, 1), (0, 2), (0, 3), (0, 4), (1, 1), (1, 2), (1, 3), (1, 4), (2, 2), (2, 3), (2, 4), (3, 3), (3, 4), (4, 4)]
    >>> list(iterable_unique_combination_generator(["A","B","C"]))
    [('A', 'A'), ('A', 'B'), ('A', 'C'), ('B', 'B'), ('B', 'C'), ('C', 'C')]
    """
    # Split the same iterable into two, so we can combine them assuming it's the same data
    iter_x, iter_y = tee(iterator, 2)
    for index, value_x in enumerate(iter_x):
        # Slice off already used value_x values from y iterator, to avoid duplicate entries
        iter_y, sliced_y = tee(iter_y, 2)
        for value_y in islice(sliced_y, index, None):
            yield value_x, value_y

if __name__ == "__main__":
    palindrome_products = []
    for x, y in iterable_unique_combination_generator(range(100,1000)):
        product = x * y
        if is_palindrome(product):
            # Can't rely on first occurance, even though x value is maximized
            # product of lower x and y values combined could still be larger.
            palindrome_products.append(product)
    print(max(palindrome_products))

# Answer: 906609