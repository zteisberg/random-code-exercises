from p_1 import is_product_multiple_of
from typing import Iterable

def is_product_multiple_of_all(product: int, divisors: Iterable) -> bool:
    """
    Given a product and list of divisors, confirm whether or not the product is a 
    multiple of all of the divisors.

    Examples:

    >>> is_product_multiple_of_all(2520, range(1,11)) # 1-10
    True
    >>> is_product_multiple_of_all(2520, range(1,12)) # 1-11
    False
    >>> is_product_multiple_of_all(15, [5,3,1])
    True
    >>> is_product_multiple_of_all(15, [5,3,2])
    False
    """
    return all(is_product_multiple_of(product, divisor) for divisor in divisors)

if __name__ == '__main__':
    i = 20
    while True:
        if is_product_multiple_of_all(i, range(1,21)):
            print(i)
            break
        i += 20

# Answer: 232792560