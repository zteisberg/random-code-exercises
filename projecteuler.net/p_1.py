def is_product_multiple_of(product: int, value: int) -> bool:
    """
    https://projecteuler.net/problem=1
    https://www.mathsisfun.com/definitions/multiple.html
    Checks if product is the result of multiplying a number (x) by any arbitrary integer.

    Examples:
    >>> is_product_multiple_of(15, 3)
    True
    >>> is_product_multiple_of(15, 5)
    True
    >>> is_product_multiple_of("15", 5)
    Traceback (most recent call last):
        ...
    TypeError: Product: str: 15 is not a valid integer
    >>> is_product_multiple_of(15, 5.0)
    Traceback (most recent call last):
        ...
    TypeError: Value: float: 5.0 is not a valid integer
    """
    if type(product) is not int:
        raise TypeError(f"Product: {type(product).__name__}: {product} is not a valid integer")
    if type(value) is not int:
        raise TypeError(f"Value: {type(value).__name__}: {value} is not a valid integer")
    return product % value == 0

if __name__ == '__main__':
    print(sum(product for product in range(0,1000) if is_product_multiple_of(product, 3) or is_product_multiple_of(product, 5)))

# Answer: 233168