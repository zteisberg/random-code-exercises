from p_1 import is_product_multiple_of
from math import sqrt, ceil
from typing import Generator, Set

def find_factors(n: int) -> Set[int]:
    """
    Find all factors of integer n quickly and return a set
    containing all possible values.

    Examples:

    >>> find_factors(15)
    {3, 5}
    >>> find_factors(100)
    {2, 4, 5, 10, 50, 20, 25}
    >>> find_factors(75)
    {25, 3, 5, 15}
    """
    factors = set()
    end = ceil(sqrt(n))
    for i in range(2, end+1):
        if is_product_multiple_of(n, i):
            factors.add(i)
            # Add compliment factor for each to set in case factor > sqrt(n)
            factors.add(n // i)
    return factors

def find_factor_generator_desc(n: int) -> Generator[int, None, None]:
    """
    Find all non-self and 1 factors of integer n, and return them in 
    a generator which yields them in descending order.
    
    Examples:
    >>> list(find_factor_generator_desc(15))
    [5, 3]
    >>> list(find_factor_generator_desc(100))
    [50, 25, 20, 10, 5, 4, 2]
    """
    # Optimization: Highest possible non-self prime factor is n // 2
    start = n // 2
    for i in range(start, 1, -1):
        if is_product_multiple_of(n, i):
            yield i

def find_factor_generator_asc(n: int) -> Generator[int, None, None]:
    """
    Find all non-self and 1 factors of integer n, and return them in 
    a generator which yields them in ascending order.
    
    Examples:
    >>> list(find_factor_generator_asc(15))
    [3, 5]
    >>> list(find_factor_generator_asc(100))
    [2, 4, 5, 10, 20, 25, 50]
    """
    # Optimization: Highest possible non-self prime factor is n // 2
    end = n // 2
    for i in range(2, end+1):
        if is_product_multiple_of(n, i):
            yield i

def is_prime(n: int) -> bool:
    """
    Returns true if integer n is a prime.

    Examples:
    >>> is_prime(3)
    True
    >>> is_prime(20)
    False
    >>> is_prime(17)
    True
    >>> is_prime(503)
    True
    """
    try:
        # Optimization: If any factor is found, this is not a prime. No need to find all factors
        # Generally will be faster to find low number factors than high numbers, so use asc generator.
        next(find_factor_generator_asc(n))
        return False
    except StopIteration:
        # If no factors found, this is prime.
        return True

if __name__ == '__main__':
    # Looking for largest factor so use desc generator and break as soon as a prime is found.
    factors = find_factors(600851475143)
    for factor in sorted(factors, reverse=True):
        if is_prime(factor):
            print(factor)
            break

# Answer: 6857