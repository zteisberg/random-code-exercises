if __name__ == '__main__':
    natural_sum_squared = sum(range(1,101)) ** 2
    natural_squares_summed = sum(x ** 2 for x in range(1,101))
    print(natural_sum_squared - natural_squares_summed)

# Answer: 25164150