from p_3 import is_prime

def get_nth_prime(n: int) -> int:
    """
    Assuming 2 is the first prime number, find the prime number at the nth position
    Examples correlate with problem description at: https://projecteuler.net/problem=7
    Examples:

    >>> get_nth_prime(1)
    2
    >>> get_nth_prime(2)
    3
    >>> get_nth_prime(6)
    13
    """
    count = 1
    i = 1
    if n == 1:
        return 2
    while count < n:
        # we can skip all even numbers succintly by assuming 2 is already counted
        i +=2
        if is_prime(i):
            count += 1
    return i

if __name__ == '__main__':
    print(get_nth_prime(100001))

# Answer: 104743